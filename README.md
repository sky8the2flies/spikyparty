################### VISION CODING {C}#####################
Vision Coding {C}
You have successfully purchased a product from Vision Coding.
Client: Pedro Carcamo
Product name:
	SpikyParty
	
Description:
	Party plugin.

Copyright:
	Read LICENSE

Install:
	Drag and drop your .JAR file into the \plugins folder of your
	minecraft server. Restart / Reload your server and be on your
	way!
	
Contact:
	Email: codingvision@gmail.com
################### VISION CODING {C}#####################