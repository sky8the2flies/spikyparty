package com.visioncoding.party.game.chromofed.game;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.GameManager;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.chromofed.utils.FireworkUtil;

/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class ChromoFedGame {
	
	public static ChromoFedGame cf = new ChromoFedGame();
	
	public static ChromoFedGame get(){
		return cf;
	}
	
	public List<Player> members;
	
	public void start(){
		GameManager.get().getGame().setType(GameType.CHROMOFLED);
		for(Player p : members){
			p.setExp(0);
			p.setHealth(20D);
			p.setFoodLevel(20);
			p.setFireTicks(0);
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			
		  p.sendMessage("The game is about to begin, please be prepared.");
			p.teleport(com.visioncoding.party.game.LocUtil.get().getLobbyLocation());
			
			ChromoFedGameTask.begin();
		  

		}
	}
	
	@SuppressWarnings("deprecation")
	public void end(){
		
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(SpikyParty.instance, new BukkitRunnable(){
			@Override
			public void run(){
				for(Player player : members){
					player.getInventory().clear();
					FireworkUtil.LaunchRandomFirework(player.getLocation().clone());
					if(members.contains(player)){
						members.remove(player);
					}
				}
			}
		},20L, 20L);
	}

}