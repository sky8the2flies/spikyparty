package com.visioncoding.party.game.chromofed.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.LocUtil;
import com.visioncoding.party.managers.Manager;

/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class ChromoFedGameTask {
	
    static List<Block> blocks = new ArrayList<Block>();
    		
	static Location loc = new Location(LocUtil.get().getChromoFedLocation().getWorld(),LocUtil.get().getChromoFedLocation().getBlockX()+5
			,LocUtil.get().getChromoFedLocation().getBlockY(),LocUtil.get().getChromoFedLocation().getBlockZ()+5);
	
	static Location loc12 = new Location(LocUtil.get().getChromoFedLocation().getWorld(),LocUtil.get().getChromoFedLocation().getBlockX()-5
			,LocUtil.get().getChromoFedLocation().getBlockY(),LocUtil.get().getChromoFedLocation().getBlockZ()-5);
	
	
	public static List<Block> blocksFromTwoPoints(Location loc1, Location loc2){
        
        loc = loc1;
        loc12 = loc2;
 
        int topBlockX = (loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
        int bottomBlockX = (loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
 
        int topBlockY = (loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
        int bottomBlockY = (loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
 
        int topBlockZ = (loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
        int bottomBlockZ = (loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
 
        for(int x = bottomBlockX; x <= topBlockX; x++)
        {
            for(int z = bottomBlockZ; z <= topBlockZ; z++)
            {
                for(int y = bottomBlockY; y <= topBlockY; y++)
                {
                    Block block = loc1.getWorld().getBlockAt(x, y, z);
                   
                    blocks.add(block);
                }
            }
        }
       
        return blocks;
    }
	
	@SuppressWarnings("deprecation")
	public static void begin(){
		
		for(Player p : ChromoFedGame.get().members){
			
		Manager.getInstance().sendTitle(p, ChatColor.GOLD + "The Game is about to begin.", "Begining in 3", 1, 3, 3);
		Manager.getInstance().sendTitle(p, ChatColor.GREEN + "The Game has Began.", "", 4, 2, 3);
		
		final Random r = new Random();
		@SuppressWarnings("unused")
		int wooltype = r.nextInt(16);
		
		for(final Block b : blocks){
			wooltype++;
				int time = r.nextInt(16);
			Manager.getInstance().sendTitle(p, b.getType().toString(), "", 1, 3, 1);
				
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SpikyParty.instance, new BukkitRunnable(){
				@Override
				public void run(){
					b.setType(Material.AIR);
					}
				},time*15);
			
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SpikyParty.instance, new BukkitRunnable(){
				@Override
				public void run(){
					int wooltype = r.nextInt(16);
					if(b.getType().equals(Material.AIR)){
						switch  (wooltype){
						case 1:
							b.setType(Material.WOOL);
							b.setData((byte)0);
						break;
						case 2:
							b.setType(Material.WOOL);
							b.setData((byte)1);
						break;
						case 3:
							b.setType(Material.WOOL);
							b.setData((byte)2);
						break;
						case 4:
							b.setType(Material.WOOL);
							b.setData((byte)3);
						break;
						case 5:
							b.setType(Material.WOOL);
							b.setData((byte)4);
						break;
						case 6:
							b.setType(Material.WOOL);
							b.setData((byte)5);
						break;
						case 7:
							b.setType(Material.WOOL);
							b.setData((byte)6);
						break;
						case 8:
							b.setType(Material.WOOL);
							b.setData((byte)7);
						break;
						case 9:
							b.setType(Material.WOOL);
							b.setData((byte)8);
						break;
						case 10:
							b.setType(Material.WOOL);
							b.setData((byte)9);
						break;
						case 11:
							b.setType(Material.WOOL);
							b.setData((byte)10);
						break;
						case 12:
							b.setType(Material.WOOL);
							b.setData((byte)11);
						break;
						case 13:
							b.setType(Material.WOOL);
							b.setData((byte)12);
						break;
						case 14:
							b.setType(Material.WOOL);
							b.setData((byte)13);
						break;
						case 15:
							b.setType(Material.WOOL);
							b.setData((byte)14);
						break;
						case 16:
							b.setType(Material.WOOL);
							b.setData((byte)15);
						break;
						
							}
						}
					}
				},time*20);
			}
		}
	}	
}
