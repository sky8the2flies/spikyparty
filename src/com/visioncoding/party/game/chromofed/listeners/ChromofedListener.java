package com.visioncoding.party.game.chromofed.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.visioncoding.party.game.Game;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.LocUtil;
import com.visioncoding.party.game.Stage;
import com.visioncoding.party.game.chromofed.game.ChromoFedGame;

/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class ChromofedListener implements Listener{
	
	private Game game;
	
	@EventHandler
	public void respawn(PlayerDeathEvent e){
		Player player = e.getEntity();
		if(game.getStage() == Stage.IN_LOBBY || game.getStage() == Stage.RESTARTING){
		player.teleport(LocUtil.get().getLobbyLocation());	
		}else if (game.getStage() == Stage.IN_GAME){
			ChromoFedGame.get().members.remove(player);
			player.setHealth(20);
			player.setFlying(true);
			player.setAllowFlight(true);
			
			for(Player p : Bukkit.getOnlinePlayers()){
				p.hidePlayer(player);
				}
			}
		}
	
	@EventHandler
	public void onbreak(BlockBreakEvent e){
		if(GameType.getType().equals(GameType.CHROMOFLED)){
		e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void damage(EntityDamageByEntityEvent e){
		if(GameType.getType().equals(GameType.CHROMOFLED)){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void place(BlockPlaceEvent e){
		if(GameType.getType().equals(GameType.CHROMOFLED)){
		e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void pickup(PlayerPickupItemEvent e){
		if(GameType.getType().equals(GameType.CHROMOFLED)){
		e.setCancelled(true);
		e.getItem().remove();
		}
	}
	
	@EventHandler
	public void food(FoodLevelChangeEvent e){
		if(GameType.getType().equals(GameType.CHROMOFLED)){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void sneak(PlayerToggleSneakEvent e){
		if(GameType.getType().equals(GameType.CHROMOFLED)){
			e.setCancelled(true);
		}
	}
}
