package com.visioncoding.party.game;

import java.util.ArrayList;
import java.util.List;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.goldguardians.GoldGuardians;
import com.visioncoding.party.game.skywars.SkyWars;

public class GameManager {
	
	private static GameManager instance;
	
	public static GameManager get() { 
		if (instance == null)
			instance = new GameManager();
		return instance;
	}
	
	public void loadGames() {
		
	}
	
	private Game game;
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	
	public Game getGame() {
		return this.game;
	}
	
	private List<GameType> hasPlayed = new ArrayList<GameType>();
	//anvillanche, skywars, gold_guardians, gridhopper, chromo
	public void next(Type type) {
		if (type == Type.ORDER) {
			if (getGame().getPrevType() == null) {
				
			} else if (getGame().getPrevType() == GameType.ANVLLANCHE) {
				if (!hasPlayed.contains(GameType.SKYWARS)) {
					hasPlayed.add(GameType.SKYWARS);
					SkyWars sw = new SkyWars(SpikyParty.get());
					sw.startSkyWars();
				} else {
					getGame().setPrevType(GameType.SKYWARS);
					next(Type.ORDER);
				}
			} else if (getGame().getPrevType() == GameType.SKYWARS) {
				if (!hasPlayed.contains(GameType.GOLD_GUARDIAN)) {
					hasPlayed.add(GameType.GOLD_GUARDIAN);
					GoldGuardians gg = new GoldGuardians(SpikyParty.get());
					gg.startGoldGuardian();
				} else {
					getGame().setPrevType(GameType.GOLD_GUARDIAN);
					next(Type.ORDER);
				}
			}
		}
	}

}
