package com.visioncoding.party.game.skywars;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.GameManager;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.Type;
import com.visioncoding.party.game.goldguardians.GoldGuardians;
import com.visioncoding.party.managers.Manager;
import com.visioncoding.party.utils.ChatUtilities;
import com.visioncoding.party.utils.ItemUtil;

public class SkyWars implements Listener {
	
	Inventory spectatormenu = Bukkit.createInventory(null, 27, ChatColor.AQUA + "Spectator Settings");
	ArrayList<String> flyon = new ArrayList<String>();
	ArrayList<String> nighton = new ArrayList<String>();
	
	
	private SpikyParty plugin;

	public SkyWars(SpikyParty plugin)
	{
		this.plugin = plugin;

	}
	
	@SuppressWarnings("deprecation")
	public void startSkyWars() {
		GameManager.get().getGame().setType(GameType.SKYWARS);
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.getInventory().clear();
			p.getInventory().setHelmet(new ItemStack(Material.AIR));
			p.getInventory().setChestplate(new ItemStack(Material.AIR));
			p.getInventory().setLeggings(new ItemStack(Material.AIR));
			p.getInventory().setBoots(new ItemStack(Material.AIR));
			Manager.getInstance().sendTitle(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Stage 2", ChatColor.GRAY + "Sky Wars", 1, 3, 3);
			ChatUtilities.sendMessage(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Stage 2: " + ChatColor.GRAY + "Sky Wars");
			
			//Teleport players to their islands here
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player player = (Player) event.getEntity();
		
		if (GameManager.get().getGame().getType() == GameType.SKYWARS) {
			
		player.setHealth(20.0);
		
		SpikyParty.getPlayers().removePlayer(player);
		SpikyParty.getSpectators().addPlayer(player);
		
		player.getInventory().clear();
		player.getInventory().setHelmet(new ItemStack(Material.AIR));
		player.getInventory().setChestplate(new ItemStack(Material.AIR));
	    player.getInventory().setLeggings(new ItemStack(Material.AIR));
	    player.getInventory().setBoots(new ItemStack(Material.AIR));
		player.updateInventory();
		
		ItemStack spectatorsettings = ItemUtil.createItemStack(new ItemStack(Material.REDSTONE_COMPARATOR), "&aSpectator Settings", "&7Right-Click to select your spectator settings!");
		player.getInventory().setItem(9, spectatorsettings);
		
		if (SpikyParty.getPlayers().getPlaying().size() == 1) {
			finish(SpikyParty.getPlayers().getPlaying().get(0));
			
			
					GameManager.get().getGame().setType(GameType.WAITING);
					Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-lobby"), SpikyParty.instance.getConfig().getInt("y-lobby"), SpikyParty.instance.getConfig().getInt("z-lobby"));
					for (Player p : Bukkit.getOnlinePlayers()) {
						p.teleport(loc);
						
					}
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
							Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 5", ChatColor.GRAY + "Next Game: Gold Guardians", 0, 1, 0);
							}
						}
					}, 1 * 20L);
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
							Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 4", ChatColor.GRAY + "Next Game: Gold Guardians", 0, 1, 0);
							}
						}
					}, 2 * 20L);
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
							Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 3", ChatColor.GRAY + "Next Game: Gold Guardians", 0, 1, 0);
							}
						}
					}, 3 * 20L);
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
							Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 2", ChatColor.GRAY + "Next Game: Gold Guardians", 0, 1, 0);
							}
						}
					}, 4 * 20L);
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
							Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 1", ChatColor.GRAY + "Next Game: Gold Guardians", 0, 1, 0);
							}
						}
					}, 5 * 20L);
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							GameManager.get().next(Type.ORDER);
						}
					}, 6 * 20L);
			
		}
		
		if (player.getKiller() instanceof Player) {
			event.setDeathMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " was elmininated by " + ChatColor.RED + player.getKiller().getName());
		} else {
			event.setDeathMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " was eliminated by " + ChatColor.RED + "Environment");
		}
		
		
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		Player player = (Player) event.getEntity();
		Player damager = (Player) event.getDamager();
		
		if (GameManager.get().getGame().getType() == GameType.SKYWARS) {
		if (damager instanceof Player) {
			if (SpikyParty.getSpectators().isSpectatorSpectating(damager)) {
				if (SpikyParty.getPlayers().isPlayerPlaying(player)) {
				event.setCancelled(true);
				}
			}
			
			if (SpikyParty.getPlayers().isPlayerPlaying(damager)) {
				if (SpikyParty.getSpectators().isSpectatorSpectating(player)) {
					event.setCancelled(true);
				}
			}
		}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		
		if (GameManager.get().getGame().getType() == GameType.SKYWARS) {
			if (SpikyParty.getPlayers().isPlayerPlaying(player)) {
				event.setCancelled(false);
			} else {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		
		if (GameManager.get().getGame().getType() == GameType.SKYWARS) {
			if (SpikyParty.getPlayers().isPlayerPlaying(player)) {
				event.setCancelled(false);
			} else {
				event.setCancelled(true);
			}
		}
		
	}
	
	public void finish(Player p) {
		
		p.getInventory().clear();
		p.getInventory().setHelmet(new ItemStack(Material.AIR));
	    p.getInventory().setChestplate(new ItemStack(Material.AIR));
    	p.getInventory().setLeggings(new ItemStack(Material.AIR));
		p.getInventory().setBoots(new ItemStack(Material.AIR));
		p.updateInventory();
		
		Manager.getInstance().sendTitle(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Winner: " + ChatColor.GOLD + p.getName(), ChatColor.GRAY + "Next Stage: Gold Guardian", 1, 3, 3);
		ChatUtilities.Broadcast(ChatColor.GOLD + "" + ChatColor.BOLD + p.getName() + ChatColor.GRAY + " has won! Next Stage: Gold Guardians");
	}
	
	@EventHandler
	public void PlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Material mat = player.getItemInHand().getType();
		
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			if(mat == Material.REDSTONE_COMPARATOR) {
				onSpectatorSettings(player);
			}
		
		}
	}
	
	public void onSpectatorSettings(Player player) {
		ArrayList<String> flyonlore = new ArrayList<String>();
		ArrayList<String> flyofflore = new ArrayList<String>();
		ArrayList<String> nightonlore = new ArrayList<String>();
		ArrayList<String> nightofflore = new ArrayList<String>();
		
		if (!flyon.contains(player.getName())) {
		ItemStack flyon = new ItemStack(Material.GOLD_BOOTS);
		ItemMeta flyonmeta = flyon.getItemMeta();
		flyonmeta.setDisplayName(ChatColor.GREEN + "Enable Speed");
		flyonlore.add(ChatColor.GRAY + "Click to enable fly speed!");
		flyonmeta.setLore(flyonlore);
		flyon.setItemMeta(flyonmeta);
		spectatormenu.setItem(12, flyon);
		} else {
		ItemStack flyoff = new ItemStack(Material.GOLD_BOOTS);
		ItemMeta flyoffmeta = flyoff.getItemMeta();
		flyoffmeta.setDisplayName(ChatColor.GREEN + "Disable Speed");
		flyofflore.add(ChatColor.GRAY + "Click to disable fly speed!");
		flyoffmeta.setLore(flyofflore);
		flyoff.setItemMeta(flyoffmeta);
		spectatormenu.setItem(12, flyoff);
		}
		
		if (!nighton.contains(player.getName())) {
		ItemStack nighton = new ItemStack(Material.ENDER_PEARL);
		ItemMeta nightonmeta = nighton.getItemMeta();
		nightonmeta.setDisplayName(ChatColor.GREEN + "Enable Night Vision");
		nightonlore.add(ChatColor.GRAY + "Click to enable night vision!");
		nightonmeta.setLore(nightonlore);
		nighton.setItemMeta(nightonmeta);
		spectatormenu.setItem(16, nighton);
		} else {
		ItemStack nightoff = new ItemStack(Material.ENDER_PEARL);
		ItemMeta nightoffmeta = nightoff.getItemMeta();
		nightoffmeta.setDisplayName(ChatColor.GREEN + "Disable Night Vision");
		nightofflore.add(ChatColor.GRAY + "Click to disable night vision!");
		nightoffmeta.setLore(nightofflore);
		nightoff.setItemMeta(nightoffmeta);
		spectatormenu.setItem(16, nightoff);
		}
		
		player.openInventory(spectatormenu);
	}
	
	
	 @EventHandler
		public void onSpectatorClick(InventoryClickEvent event) {
			Player player = (Player) event.getWhoClicked();
			
			if (event.getInventory().getName().equalsIgnoreCase(ChatColor.AQUA + "Gizmo Menu")) {
				
				if (event.getCurrentItem().getType() == Material.GOLD_BOOTS) {
					event.setCancelled(true);
					player.closeInventory();
					
					if (flyon.contains(player.getName())) {
						flyon.remove(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have disabled speed!");
						
						player.removePotionEffect(PotionEffectType.SPEED);
						player.setFlySpeed(1);
					} else {
						flyon.add(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have enabled speed!");
						
						player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999999, 99999999));
						player.setFlySpeed(3);
					}
					
					}
				
				if (event.getCurrentItem().getType() == Material.ENDER_PEARL) {
					event.setCancelled(true);
					player.closeInventory();
					
					if (nighton.contains(player.getName())) {
						nighton.remove(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have disabled night vision!");
						
						player.removePotionEffect(PotionEffectType.NIGHT_VISION);
					} else {
						nighton.add(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have enabled night vision!");
						
						player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 9999999, 99999999));
					}
				}
			}
	 }

}
