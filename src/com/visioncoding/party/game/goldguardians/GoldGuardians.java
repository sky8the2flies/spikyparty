package com.visioncoding.party.game.goldguardians;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.GameManager;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.Type;
import com.visioncoding.party.managers.Manager;
import com.visioncoding.party.utils.ChatUtilities;
import com.visioncoding.party.utils.ItemUtil;

public class GoldGuardians implements Listener {
	
	Inventory spectatormenu = Bukkit.createInventory(null, 27, ChatColor.AQUA + "Spectator Settings");
	ArrayList<String> flyon = new ArrayList<String>();
	ArrayList<String> nighton = new ArrayList<String>();
	static ArrayList<Entity> zombies = new ArrayList<Entity>();
	static ArrayList<String> lost = new ArrayList<String>();

	private static SpikyParty plugin;

	public GoldGuardians(SpikyParty plugin)
	{
		GoldGuardians.plugin = plugin;

	}
	
	@SuppressWarnings({ "deprecation" })
	public void startGoldGuardian() {
		GameManager.get().getGame().setType(GameType.GOLD_GUARDIAN);
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.getInventory().clear();
			p.getInventory().setHelmet(new ItemStack(Material.AIR));
			p.getInventory().setChestplate(new ItemStack(Material.AIR));
			p.getInventory().setLeggings(new ItemStack(Material.AIR));
			p.getInventory().setBoots(new ItemStack(Material.AIR));
			Manager.getInstance().sendTitle(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Stage 3", ChatColor.GRAY + "Gold Guardians", 1, 3, 3);
			ChatUtilities.sendMessage(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Stage 3: " + ChatColor.GRAY + "Gold Guardians");
			
			//Teleport players here
			
			start(p);
		}
	}
	
	public void start(Player player) {
		
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-goldblock"), SpikyParty.instance.getConfig().getInt("y-goldblock"), SpikyParty.instance.getConfig().getInt("z-goldblock"));
		loc.getWorld().getBlockAt(loc).setType(Material.GOLD_BLOCK);
		
		wave1(player);
		
		/*
		Wave 1: Leather Armor - 10
		Wave 2: Gold Armor - 12
		Wave 3: Iron Armor + Pigmen - 14
		Wave 4: Diamond Armor + Pigmen - 10
		 */
		
	}
	
	@SuppressWarnings("deprecation")
	public void wave1(final Player player) {
		//open door 1
		final Location door1 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door1"), SpikyParty.instance.getConfig().getInt("y-door1"), SpikyParty.instance.getConfig().getInt("z-door1"));
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-goldblock"), SpikyParty.instance.getConfig().getInt("y-goldblock"), SpikyParty.instance.getConfig().getInt("z-goldblock"));
		
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Wave 1: Leather Armored Zombies");
		
		Manager.getInstance().sendTitle(player, ChatColor.GOLD + "" + ChatColor.BOLD + "Wave 1", ChatColor.GRAY + "Leather Armored Zombies", 1, 3, 3);
		
		final WitherSkull skull = (WitherSkull) player.getWorld().spawn(loc.add(0, 1, 0), WitherSkull.class);
		skull.setDirection(new Vector(0, 0, 0));
		skull.setVelocity(new Vector(0, 0, 0));
		
				//1
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
		
				//2
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//3
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//4
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//5
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//6
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//7
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//8
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//9
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//10
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
		
		if (zombies.isEmpty()) {
			for (Player p : Bukkit.getOnlinePlayers()) {
			if (!lost.contains(p.getName())) {
			zombies.clear();
			skull.remove();
			wave2(player);
			}
			}
		}
		
		if (skull.getNearbyEntities(2, 2, 2).contains(EntityType.ZOMBIE) || skull.getNearbyEntities(2, 2, 2).contains(EntityType.PIG_ZOMBIE)) {
			skull.remove();
			zombies.clear();
			for (Player p : Bukkit.getOnlinePlayers()) {
				lost.add(p.getName());
			}
			finishlost(player);
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void wave2(final Player player) {
		//open door 2
		final Location door1 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door1"), SpikyParty.instance.getConfig().getInt("y-door1"), SpikyParty.instance.getConfig().getInt("z-door1"));
		final Location door2 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door2"), SpikyParty.instance.getConfig().getInt("y-door2"), SpikyParty.instance.getConfig().getInt("z-door2"));
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-goldblock"), SpikyParty.instance.getConfig().getInt("y-goldblock"), SpikyParty.instance.getConfig().getInt("z-goldblock"));
		
		
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Wave 2: Gold Armored Zombies");
		Manager.getInstance().sendTitle(player, ChatColor.GOLD + "" + ChatColor.BOLD + "Wave 2", ChatColor.GRAY + "Gold Armored Zombies", 1, 3, 3);
		
		final WitherSkull skull = (WitherSkull) player.getWorld().spawn(loc.add(0, 1, 0), WitherSkull.class);
		skull.setDirection(new Vector(0, 0, 0));
		skull.setVelocity(new Vector(0, 0, 0));
		
		//1
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);

		//2
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//3
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//4
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//5
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//6
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//7
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//8
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//9
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//10
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//11
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//12
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				/*
				 Next Door Spawn
				 */
				
				//1
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);

				//2
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//3
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//4
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//5
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//6
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//7
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//8
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//9
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//10
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//11
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//12
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);

						if (zombies.isEmpty()) {
							for (Player p : Bukkit.getOnlinePlayers()) {
							if (!lost.contains(p.getName())) {
							zombies.clear();
							skull.remove();
							wave3(player);
							}
							}
						}
						
						if (skull.getNearbyEntities(2, 2, 2).contains(EntityType.ZOMBIE) || skull.getNearbyEntities(2, 2, 2).contains(EntityType.PIG_ZOMBIE)) {
							skull.remove();
							zombies.clear();
							for (Player p : Bukkit.getOnlinePlayers()) {
								lost.add(p.getName());
							}
							finishlost(player);
						}
		
	}
	
	@SuppressWarnings("deprecation")
	public void wave3(final Player player) {
		//open door 3
		final Location door1 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door1"), SpikyParty.instance.getConfig().getInt("y-door1"), SpikyParty.instance.getConfig().getInt("z-door1"));
		final Location door2 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door2"), SpikyParty.instance.getConfig().getInt("y-door2"), SpikyParty.instance.getConfig().getInt("z-door2"));
		final Location door3 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door3"), SpikyParty.instance.getConfig().getInt("y-door3"), SpikyParty.instance.getConfig().getInt("z-door3"));
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-goldblock"), SpikyParty.instance.getConfig().getInt("y-goldblock"), SpikyParty.instance.getConfig().getInt("z-goldblock"));
		
		
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Wave 3: Iron Armored Zombies & Pigmen");
		Manager.getInstance().sendTitle(player, ChatColor.GOLD + "" + ChatColor.BOLD + "Wave 1", ChatColor.GRAY + "Iron Armored Zombies & Pigmen", 1, 3, 3);
		
		final WitherSkull skull = (WitherSkull) player.getWorld().spawn(loc.add(0, 1, 0), WitherSkull.class);
		skull.setDirection(new Vector(0, 0, 0));
		skull.setVelocity(new Vector(0, 0, 0));
		
		//1
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);

				//2
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//3
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//4
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//5
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//6
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//7
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//8
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//9
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//10
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//11
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//12
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//13
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//14
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						/*
						 Door 2
						 */
						
						//1
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);

						//2
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//3
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//4
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//5
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//6
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//7
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//8
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//9
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//10
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
								
								z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
								z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
								z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
								z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
								z1.setTarget((LivingEntity) skull);
								zombies.add(z1);
							}
						}, 3 * 20L);
						
						//11
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//12
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//13
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//14
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								/*
								 Door 3
								 */
								
								//1
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);

								//2
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//3
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//4
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//5
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//6
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//7
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//8
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//9
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//10
								Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
										
										z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
										z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
										z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
										z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
										z1.setTarget((LivingEntity) skull);
										zombies.add(z1);
									}
								}, 3 * 20L);
								
								//11
										Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
											public void run() {
												Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
												
												z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
												z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
												z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
												z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
												z1.setTarget((LivingEntity) skull);
												zombies.add(z1);
											}
										}, 3 * 20L);
										
										//12
										Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
											public void run() {
												Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
												
												z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
												z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
												z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
												z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
												z1.setTarget((LivingEntity) skull);
												zombies.add(z1);
											}
										}, 3 * 20L);
										
										//13
										Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
											public void run() {
												Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
												
												z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
												z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
												z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
												z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
												z1.setTarget((LivingEntity) skull);
												zombies.add(z1);
											}
										}, 3 * 20L);
										
										//14
										Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
											public void run() {
												Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
												
												z1.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
												z1.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
												z1.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
												z1.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
												z1.setTarget((LivingEntity) skull);
												zombies.add(z1);
											}
										}, 3 * 20L);

										if (zombies.isEmpty()) {
											for (Player p : Bukkit.getOnlinePlayers()) {
											if (!lost.contains(p.getName())) {
											zombies.clear();
											skull.remove();
											wave4(player);
											}
											}
										}
										
										if (skull.getNearbyEntities(2, 2, 2).contains(EntityType.ZOMBIE) || skull.getNearbyEntities(2, 2, 2).contains(EntityType.PIG_ZOMBIE)) {
											skull.remove();
											zombies.clear();
											for (Player p : Bukkit.getOnlinePlayers()) {
												lost.add(p.getName());
											}
											finishlost(player);
										}
	
	}

	@SuppressWarnings("deprecation")
	public void wave4(final Player player) {
		//open door 4
		final Location door1 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door1"), SpikyParty.instance.getConfig().getInt("y-door1"), SpikyParty.instance.getConfig().getInt("z-door1"));
		final Location door2 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door2"), SpikyParty.instance.getConfig().getInt("y-door2"), SpikyParty.instance.getConfig().getInt("z-door2"));
		final Location door3 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door3"), SpikyParty.instance.getConfig().getInt("y-door3"), SpikyParty.instance.getConfig().getInt("z-door3"));
		final Location door4 = new Location(player.getWorld(), SpikyParty.instance.getConfig().getInt("x-door4"), SpikyParty.instance.getConfig().getInt("y-door4"), SpikyParty.instance.getConfig().getInt("z-door4"));
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-goldblock"), SpikyParty.instance.getConfig().getInt("y-goldblock"), SpikyParty.instance.getConfig().getInt("z-goldblock"));
	
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Final Wave - 4: Diamond Armored Zombies & Pigmen");
		Manager.getInstance().sendTitle(player, ChatColor.GOLD + "" + ChatColor.BOLD + "Final Wave - 4", ChatColor.GRAY + "Diamond Armored Zombies & Pigmen", 1, 3, 3);
		
		final WitherSkull skull = (WitherSkull) player.getWorld().spawn(loc.add(0, 1, 0), WitherSkull.class);
		skull.setDirection(new Vector(0, 0, 0));
		skull.setVelocity(new Vector(0, 0, 0));
		
		//1
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);

		//2
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//3
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//4
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//5
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//6
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//7
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//8
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
			 	zombies.add(z1);
			}
		}, 3 * 20L);
		
		//9
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		//10
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Zombie z1 = (Zombie) player.getWorld().spawnEntity(door1, EntityType.PIG_ZOMBIE);
				
				z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				z1.setTarget((LivingEntity) skull);
				zombies.add(z1);
			}
		}, 3 * 20L);
		
		/*
		 Door 2
		 */
		
		//1
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);

				//2
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//3
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//4
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//5
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//6
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//7
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//8
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
					 	zombies.add(z1);
					}
				}, 3 * 20L);
				
				//9
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//10
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door2, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				/*
				 Door 3
				 */
				
				//1
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);

				//2
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//3
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//4
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//5
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//6
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//7
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//8
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
					 	zombies.add(z1);
					}
				}, 3 * 20L);
				
				//9
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//10
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door3, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				/*
				 Door 4
				 */
				
				//1
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);

				//2
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//3
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//4
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//5
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//6
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//7
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//8
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
					 	zombies.add(z1);
					}
				}, 3 * 20L);
				
				//9
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);
				
				//10
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Zombie z1 = (Zombie) player.getWorld().spawnEntity(door4, EntityType.PIG_ZOMBIE);
						
						z1.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						z1.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z1.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z1.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z1.setTarget((LivingEntity) skull);
						zombies.add(z1);
					}
				}, 3 * 20L);

				if (zombies.isEmpty()) {
					for (Player p : Bukkit.getOnlinePlayers()) {
					if (!lost.contains(p.getName())) {
					zombies.clear();
					skull.remove();
					finishwin(player);
					
					
					
					}
					}
				}
				
				if (skull.getNearbyEntities(2, 2, 2).contains(EntityType.ZOMBIE) || skull.getNearbyEntities(2, 2, 2).contains(EntityType.PIG_ZOMBIE)) {
					skull.remove();
					zombies.clear();
					for (Player p : Bukkit.getOnlinePlayers()) {
						lost.add(p.getName());
					}
					finishlost(player);
					
					
				}
		
	}
	
	
	 
	@EventHandler
	public void onZombieDeath(EntityDeathEvent event) {
		Entity entity = event.getEntity();
		if (entity.getType() == EntityType.ZOMBIE || entity.getType() == EntityType.PIG_ZOMBIE) {
			zombies.remove(entity);
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player player = (Player) event.getEntity();
		
		if (GameManager.get().getGame().getType() == GameType.SKYWARS) {
			
		player.setHealth(20.0);
		
		SpikyParty.getPlayers().removePlayer(player);
		SpikyParty.getSpectators().addPlayer(player);
		
		player.getInventory().clear();
		player.getInventory().setHelmet(new ItemStack(Material.AIR));
		player.getInventory().setChestplate(new ItemStack(Material.AIR));
	    player.getInventory().setLeggings(new ItemStack(Material.AIR));
	    player.getInventory().setBoots(new ItemStack(Material.AIR));
		player.updateInventory();
		
		ItemStack spectatorsettings = ItemUtil.createItemStack(new ItemStack(Material.REDSTONE_COMPARATOR), "&aSpectator Settings", "&7Right-Click to select your spectator settings!");
		player.getInventory().setItem(9, spectatorsettings);
		
		if (SpikyParty.getPlayers().getPlaying().size() == 0) {
			finishlost(player);
			
		}
		}	
	}
	
	@SuppressWarnings("deprecation")
	public void finishlost(Player p) {
		
		p.getInventory().clear();
		p.getInventory().setHelmet(new ItemStack(Material.AIR));
	    p.getInventory().setChestplate(new ItemStack(Material.AIR));
    	p.getInventory().setLeggings(new ItemStack(Material.AIR));
		p.getInventory().setBoots(new ItemStack(Material.AIR));
		p.updateInventory();
		
		Manager.getInstance().sendTitle(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Winner: Zombies, Loser: Players", ChatColor.GRAY + "Next Stage: Grid Hopper", 1, 3, 3);
		ChatUtilities.Broadcast(ChatColor.GOLD + "" + ChatColor.BOLD + "Zombies" + ChatColor.GRAY + " has won! Next Stage: Grid Hopper");
		
		GameManager.get().getGame().setType(GameType.WAITING);
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-lobby"), SpikyParty.instance.getConfig().getInt("y-lobby"), SpikyParty.instance.getConfig().getInt("z-lobby"));
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.teleport(loc);
			
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 5", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 1 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 4", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 2 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 3", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 3 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 2", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 4 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 1", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 5 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				GameManager.get().next(Type.ORDER);
			}
		}, 6 * 20L);
	}
	
	@SuppressWarnings("deprecation")
	public void finishwin(Player p) {
		
		p.getInventory().clear();
		p.getInventory().setHelmet(new ItemStack(Material.AIR));
	    p.getInventory().setChestplate(new ItemStack(Material.AIR));
    	p.getInventory().setLeggings(new ItemStack(Material.AIR));
		p.getInventory().setBoots(new ItemStack(Material.AIR));
		p.updateInventory();
		
		Manager.getInstance().sendTitle(p, ChatColor.GOLD + "" + ChatColor.BOLD + "Winner: Players, Loser: Zombies", ChatColor.GRAY + "Next Stage: Grid Hopper", 1, 3, 3);
		ChatUtilities.Broadcast(ChatColor.GOLD + "" + ChatColor.BOLD + "Players" + ChatColor.GRAY + " has won! Next Stage: Grid Hopper");
		
		GameManager.get().getGame().setType(GameType.WAITING);
		Location loc = new Location(Bukkit.getWorld("world"), SpikyParty.instance.getConfig().getInt("x-lobby"), SpikyParty.instance.getConfig().getInt("y-lobby"), SpikyParty.instance.getConfig().getInt("z-lobby"));
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.teleport(loc);
			
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 5", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 1 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 4", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 2 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 3", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 3 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 2", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 4 * 20L);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
				Manager.getInstance().sendTitle(p, ChatColor.GREEN + "" + ChatColor.BOLD + "Starting In 1", ChatColor.GRAY + "Next Game: Grid Hopper", 1, 3, 3);
				}
			}
		}, 5 * 20L);
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		Player player = (Player) event.getEntity();
		Player damager = (Player) event.getDamager();
		
		if (GameManager.get().getGame().getType() == GameType.GOLD_GUARDIAN) {
		if (damager instanceof Player) {
			if (SpikyParty.getPlayers().isPlayerPlaying(player)) {
				if (SpikyParty.getPlayers().isPlayerPlaying(damager)) {
					event.setCancelled(true);
				}
			}
			
			if (SpikyParty.getSpectators().isSpectatorSpectating(damager)) {
				if (SpikyParty.getPlayers().isPlayerPlaying(player)) {
				event.setCancelled(true);
				}
			}
			
			if (SpikyParty.getPlayers().isPlayerPlaying(damager)) {
				if (SpikyParty.getSpectators().isSpectatorSpectating(player)) {
					event.setCancelled(true);
				}
			}
			
		}
		
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		
		if (GameManager.get().getGame().getType() == GameType.GOLD_GUARDIAN) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
	
		if (GameManager.get().getGame().getType() == GameType.GOLD_GUARDIAN) {
			event.setCancelled(true);
		}
		
	}
	
	@EventHandler
	public void PlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Material mat = player.getItemInHand().getType();
		
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			if(mat == Material.REDSTONE_COMPARATOR) {
				onSpectatorSettings(player);
			}
		
		}
	}
	
	public void onSpectatorSettings(Player player) {
		ArrayList<String> flyonlore = new ArrayList<String>();
		ArrayList<String> flyofflore = new ArrayList<String>();
		ArrayList<String> nightonlore = new ArrayList<String>();
		ArrayList<String> nightofflore = new ArrayList<String>();
		
		if (!flyon.contains(player.getName())) {
		ItemStack flyon = new ItemStack(Material.GOLD_BOOTS);
		ItemMeta flyonmeta = flyon.getItemMeta();
		flyonmeta.setDisplayName(ChatColor.GREEN + "Enable Speed");
		flyonlore.add(ChatColor.GRAY + "Click to enable fly speed!");
		flyonmeta.setLore(flyonlore);
		flyon.setItemMeta(flyonmeta);
		spectatormenu.setItem(12, flyon);
		} else {
		ItemStack flyoff = new ItemStack(Material.GOLD_BOOTS);
		ItemMeta flyoffmeta = flyoff.getItemMeta();
		flyoffmeta.setDisplayName(ChatColor.GREEN + "Disable Speed");
		flyofflore.add(ChatColor.GRAY + "Click to disable fly speed!");
		flyoffmeta.setLore(flyofflore);
		flyoff.setItemMeta(flyoffmeta);
		spectatormenu.setItem(12, flyoff);
		}
		
		if (!nighton.contains(player.getName())) {
		ItemStack nighton = new ItemStack(Material.ENDER_PEARL);
		ItemMeta nightonmeta = nighton.getItemMeta();
		nightonmeta.setDisplayName(ChatColor.GREEN + "Enable Night Vision");
		nightonlore.add(ChatColor.GRAY + "Click to enable night vision!");
		nightonmeta.setLore(nightonlore);
		nighton.setItemMeta(nightonmeta);
		spectatormenu.setItem(16, nighton);
		} else {
		ItemStack nightoff = new ItemStack(Material.ENDER_PEARL);
		ItemMeta nightoffmeta = nightoff.getItemMeta();
		nightoffmeta.setDisplayName(ChatColor.GREEN + "Disable Night Vision");
		nightofflore.add(ChatColor.GRAY + "Click to disable night vision!");
		nightoffmeta.setLore(nightofflore);
		nightoff.setItemMeta(nightoffmeta);
		spectatormenu.setItem(16, nightoff);
		}
		
		player.openInventory(spectatormenu);
	}
	
	
	 @EventHandler
		public void onSpectatorClick(InventoryClickEvent event) {
			Player player = (Player) event.getWhoClicked();
			
			if (event.getInventory().getName().equalsIgnoreCase(ChatColor.AQUA + "Gizmo Menu")) {
				
				if (event.getCurrentItem().getType() == Material.GOLD_BOOTS) {
					event.setCancelled(true);
					player.closeInventory();
					
					if (flyon.contains(player.getName())) {
						flyon.remove(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have disabled speed!");
						
						player.removePotionEffect(PotionEffectType.SPEED);
						player.setFlySpeed(1);
					} else {
						flyon.add(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have enabled speed!");
						
						player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999999, 99999999));
						player.setFlySpeed(3);
					}
					
					}
				
				if (event.getCurrentItem().getType() == Material.ENDER_PEARL) {
					event.setCancelled(true);
					player.closeInventory();
					
					if (nighton.contains(player.getName())) {
						nighton.remove(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have disabled night vision!");
						
						player.removePotionEffect(PotionEffectType.NIGHT_VISION);
					} else {
						nighton.add(player.getName());
						ChatUtilities.sendMessage(player, ChatColor.YELLOW + "You have enabled night vision!");
						
						player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 9999999, 99999999));
					}
				}
			}
	 }

}
