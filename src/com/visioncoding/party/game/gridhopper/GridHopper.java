package com.visioncoding.party.game.gridhopper;

import org.bukkit.Location;
import org.bukkit.Material;

import com.visioncoding.party.game.Game;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.Stage;

public class GridHopper {
	
	private Game game;
	private Location center;
	
	public GridHopper(Game game, Location center) {
		this.setGame(game);
		this.setCenter(center);
		
		//Run update blocks
	}
	
	public void begin() {
		if (game.start()) {
			//Start code
		} else {
			game.setType(GameType.WAITING);
			game.setStage(Stage.IN_LOBBY);
		}
	}
	
	public void sendBeamLower(String direction, Location loc) {
		double x = loc.getX();
		double z = loc.getZ();
		if (direction == "north") {
			//x- y-
			//loc x-
			for (int i = 0; loc.getWorld().getBlockAt(new Location(loc.getWorld(), x-i, loc.getY(), loc.getZ())).getType() == Material.AIR; i++) {
				for (int ii = 0; loc.getWorld().getBlockAt(new Location(loc.getWorld(), x-ii, loc.getY(), loc.getZ())).getType() == Material.AIR; i--) {
					loc.getWorld().getBlockAt(new Location(loc.getWorld(), x-ii, loc.getY(), loc.getZ())).setType(Material.GLASS);
					loc.getWorld().getBlockAt(new Location(loc.getWorld(), x-i, loc.getY(), loc.getZ())).setType(Material.GLASS);
				}
			}
		} else if (direction == "east") {
			//x- y+
			//loc y-
		} else if (direction == "south") {
			//x+ y+
			//loc x+
		} else if (direction == "west") {
			//x+ y-
			//loc y+
		} else sendBeamLower("north", loc);
	}
	
	public void sendBeamUpper(String direction) {
		if (direction == "north") {
			
		} else if (direction == "east") {
			
		} else if (direction == "south") {
			
		} else if (direction == "west") {
			
		} else sendBeamUpper("north");
	}
	
	public Location getCenter() {
		return center;
	}

	public void setCenter(Location center) {
		this.center = center;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

}
