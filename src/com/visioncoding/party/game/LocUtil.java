package com.visioncoding.party.game;

import org.bukkit.Location;

/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class LocUtil {
	
	private static LocUtil lu = new LocUtil ();
	
	public static LocUtil get(){
		return lu;
	}
	
	public Location lobbyLocation,chromoFedLocation,anvilancheLocation;

	public Location getLobbyLocation() {
		return lobbyLocation;
	}

	public void setLobbyLocation(Location lobbyLocation) {
		this.lobbyLocation = lobbyLocation;
	}

	public Location getChromoFedLocation() {
		return chromoFedLocation;
	}

	public void setChromoFedLocation(Location chromoFedLocation) {
		this.chromoFedLocation = chromoFedLocation;
	}

	public Location getAnvilancheLocation() {
		return anvilancheLocation;
	}

	public void setAnvilancheLocation(Location anvilancheLocation) {
		this.anvilancheLocation = anvilancheLocation;
	}
	

}
