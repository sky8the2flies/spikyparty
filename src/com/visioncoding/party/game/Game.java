package com.visioncoding.party.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class Game {
	
	private GameType prevType;	
	private GameType type;	
	private String displayName;
	private Stage stage;
	
	int MIN_PLAYERS = 4;
	int MAX_PLAYERS = 6;
	
	private List<String> players = new ArrayList<String>();
	
	public Game(GameType type, String displayName) {
		this.setType(type);
		this.setDisplayName(displayName);
	}
	
	public void addPlayer(Player player) {
		if (!players.contains(player.getName())) {
			players.add(player.getName());
		}
	}
	
	public void removePlayer(Player player) {
		if (players.contains(player.getName())) {
			players.remove(player.getName());
		}
	}
	
	
	public boolean start() {
		if (getPlayers().size() >= this.MIN_PLAYERS && getPlayers().size() <= this.MAX_PLAYERS) 
			return true;
		return false;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public GameType getType() {
		return type;
	}

	public void setType(GameType type) {
		if (this.type != type)
			this.setPrevType(this.type);
		this.type = type;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public List<String> getPlayers() {
		return players;
	}

	public GameType getPrevType() {
		return prevType;
	}

	public void setPrevType(GameType prevType) {
		this.prevType = prevType;
	}

}
