package com.visioncoding.party.game.anvilaunche.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.visioncoding.party.game.LocUtil;


/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class AnvilauncheGameTask {
	
	public static boolean startFalling = false;
	
	public static void begin(){
		startFalling = true;
	}
	
	public static void end(){
		startFalling = false;
		Bukkit.broadcastMessage(AnvilauncheGame.members.toString() + " has won the game.");
		for(Player player : Bukkit.getOnlinePlayers()){
			player.teleport(LocUtil.get().getLobbyLocation());
		}
	}

}