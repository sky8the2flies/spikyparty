package com.visioncoding.party.game.anvilaunche.game;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.GameManager;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.LocUtil;


/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class AnvilauncheGame {
	
	public static List<Player> members;
	
	@SuppressWarnings("deprecation")
	public void start(){
		GameManager.get().getGame().setType(GameType.ANVLLANCHE);
		
	for(Player p : members){
		p.setExp(0);
		p.setHealth(20);
		p.setFoodLevel(20);
		p.setFireTicks(0);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.sendMessage("Anvilaunche is about to begin get ready.");
		p.teleport(LocUtil.get().getAnvilancheLocation());
		AnvilauncheGameTask.begin();
		
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(SpikyParty.instance, new BukkitRunnable(){
			@Override
			public void run(){
				if(members.size() == 1){
					end();
				}
			}
		},20L, 20L);
		}	
	}
	
	public static void end(){
		AnvilauncheGameTask.end();
	}

}
