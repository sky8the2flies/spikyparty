package com.visioncoding.party.game.anvilaunche.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.anvilaunche.game.AnvilauncheGame;
import com.visioncoding.party.game.anvilaunche.game.AnvilauncheGameTask;

/*
* ******************************************************** *
* Copyright '�' frostythedev 2014. All Rights Reserved!
* Any code contained within this document, and any assosiated API's with similar branding
* are the sole property of frostythedev. Distribution, reproduction, taking snippets or
* claiming any content as your own will break the terms of this licence, and void 
* any agreement  with you, the third party.
* Thank You!
* ******************************************************** *
*/

public class AnvilauncheListener implements Listener{
	
	@EventHandler
	public void place(BlockPlaceEvent e){
		if(GameType.getType().equals(GameType.ANVLLANCHE)){
			e.setCancelled(true);
		}
		
		if(e.getPlayer().isOp()){
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onbreak(BlockBreakEvent e){
		if(GameType.getType().equals(GameType.ANVLLANCHE)){
			e.setCancelled(true);
		}
		
		if(e.getPlayer().isOp()){
			e.setCancelled(false);
			}
		}
	
	@EventHandler
	public void pickup(PlayerPickupItemEvent e){
		if(GameType.getType().equals(GameType.ANVLLANCHE)){
			e.setCancelled(true);
		}
		
		if(e.getPlayer().isOp()){
			e.setCancelled(false);
				}
			}
	
	@EventHandler
	public void death(PlayerDeathEvent e){
		if(GameType.getType().equals(GameType.ANVLLANCHE)){
			e.getEntity().setHealth(20);
			AnvilauncheGame.members.remove(e);
			for(Player p : Bukkit.getOnlinePlayers()){
				p.hidePlayer(e.getEntity());
			}
			
			e.getEntity().setFlying(true);
			e.getEntity().setAllowFlight(true);
			}
		}
	
	@EventHandler
	public void damage(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player){
			if(GameType.getType().equals(GameType.ANVLLANCHE)){
				e.setCancelled(true);
				}
			}
		}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void move(PlayerMoveEvent e){
		final Player player = e.getPlayer();
		if(AnvilauncheGameTask.startFalling){
			Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(SpikyParty.instance, new BukkitRunnable(){
				@Override
				public void run(){
					Location anvilLocation;
					
					anvilLocation = new Location (player.getWorld(), player.getLocation().getX(),player.getLocation().getY()+2.5,player.getLocation().getZ());
					player.getWorld().spawnFallingBlock(anvilLocation, Material.ANVIL, (byte) 0);
					anvilLocation = new Location (player.getWorld(), player.getLocation().getX(),player.getLocation().getY(),player.getLocation().getZ());
					anvilLocation.getBlock().breakNaturally();
					anvilLocation.getBlock().getDrops().clear();
					}
				},35,35);
			}
		}
	
	@EventHandler
	public void touch(PlayerMoveEvent e){
		Player player = e.getPlayer();
			if(player.getLocation().getBlock().equals(Material.ANVIL)){
				player.sendMessage("You have died.");
				player.setHealth(0);
			}
		}
	
	@EventHandler
	public void onEntityChange(EntityChangeBlockEvent event){
	if(event.getEntity() instanceof FallingBlock){
	if(event.getTo() == Material.ANVIL){
	event.getBlock().setType(Material.AIR);
				}
			}
		}
	}