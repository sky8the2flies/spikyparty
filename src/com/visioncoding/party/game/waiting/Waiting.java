package com.visioncoding.party.game.waiting;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.Game;
import com.visioncoding.party.game.GameManager;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.game.Type;

public class Waiting {
	
	private Game game;
	private Location center;
	
	List<Block> allBlocks = new ArrayList<Block>();
	
	public Waiting(Game game, Location center) {
		if (game.getType() == GameType.WAITING) {
			this.setGame(game);
			this.setCenter(center);
			for (int x = getCenter().getBlockX() - 5; x < getCenter().getBlockX() + 5; x++) {
				for (int z = getCenter().getBlockZ() - 5; z < getCenter().getBlockZ() + 5; z++) {
					Block block = new Location(getCenter().getWorld(), x, getCenter().getY(), z).getBlock();
					allBlocks.add(block);
				}
			}
			
		} else {
			GameManager.get().next(Type.ORDER);
		}
	}
	
	public void startWaiting() {
		SpikyParty.get().getServer().getScheduler().scheduleSyncDelayedTask(SpikyParty.get(), new Runnable() {
			public void run() {
				Location location = allBlocks.get(randomNum(0,allBlocks.size())).getLocation();
				location.getBlock().setType(randomMat());
			}
		}, 10L);
	}
	
	public Material randomMat() {
		int i = randomNum(1, 10);
		if (i == 1) {
			return Material.REDSTONE_BLOCK;
		} else if (i == 2) {
			return Material.COBBLESTONE;
		} else if (i == 3) {
			return Material.BURNING_FURNACE;
		} else if (i == 4) {
			return Material.STAINED_GLASS;
		} else if (i == 5) {
			return Material.WOOL;
		} else 
			return Material.SLIME_BLOCK;		
	}
	
	public int randomNum(int lower, int upper) {
		Random random = new Random();
		int number = random.nextInt((upper - lower) + 1) + lower;
		return number;
	}

	public Location getCenter() {
		return center;
	}

	public void setCenter(Location center) {
		this.center = center;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

}
