package com.visioncoding.party.game;


public enum MapType {
	
	JUNGLE, RAINBOW, SANDVILLE, SKYPIA, CANDY;
	
	private static MapType map;
	
	public static MapType getMap(){
		return map;
	}
	
	public static void setMap(MapType map){
		MapType.map = map;
	}

}
