package com.visioncoding.party.game;

import org.bukkit.ChatColor;

public enum Stage {
	
	IN_LOBBY(ChatColor.GRAY), IN_GAME(ChatColor.YELLOW), RESTARTING(ChatColor.RED);
	
	private ChatColor color;
	
	Stage(ChatColor color) {
		this.setColor(color);
	}

	public ChatColor getColor() {
		return color;
	}

	public void setColor(ChatColor color) {
		this.color = color;
	}

}
