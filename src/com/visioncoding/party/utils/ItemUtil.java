package com.visioncoding.party.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtil {
	
	public static ItemStack createItemStack(ItemStack stack, String name, String... lores) {
		ItemMeta im = stack.getItemMeta();
		im.setDisplayName(ChatUtilities.format(name));
		List<String> allLores = new ArrayList<String>();
		for (String lore : lores) {
			allLores.add(lore);
		}
		im.setLore(allLores);
		stack.setItemMeta(im);
		return stack;
	}

}
