package com.visioncoding.party.utils;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.bukkit.ChatColor;

public class ScoreboardUtil {
	 
	 ScoreboardManager manager = Bukkit.getScoreboardManager();
	 Scoreboard board = manager.getNewScoreboard();
	 Objective objective = board.registerNewObjective("test", "dummy");
	 
	 public ScoreboardUtil(String displayName, boolean saveScore) {
	  objective.setDisplayName(displayName);
	  if (saveScore)
	   ScoreboardManagers.getInstance().addScoreboard(displayName, this);
	 }
	 
	 public void setDisplayName(String name) {
	  objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
	 }
	 
	 public String getDisplayName() {
	  return objective.getDisplayName();
	 }
	 
	 public Team registerNewTeam(String teamName) { 
	  return board.registerNewTeam(teamName);
	 }
	 
	 public Team getTeam(String teamName) {
	  return board.getTeam(teamName);
	 }
	 
	 public Score getScore(String string) {
	  return objective.getScore(Bukkit.getOfflinePlayer(string));
	 }
	 
	 public void setScore(String string, int amount) {
	  getScore(ChatColor.translateAlternateColorCodes('&', string)).setScore(amount);
	 }
	 
	 public void removeScore(String string) {
	  getScoreboard().resetScores(Bukkit.getOfflinePlayer(string));
	 }
	 
	 public void setDisplaySlot(DisplaySlot slot) {
	  objective.setDisplaySlot(slot);
	 }
	 
	 public void updateScore(String score1, String score2, int score) {
	  setScore(score2, score);
	  removeScore(score1);
	 }
	 
	 public Scoreboard getScoreboard() {
	  return board;
	 }

	}
