package com.visioncoding.party.utils;

import java.util.HashMap;


public class ScoreboardManagers {
	 
	 private HashMap<String, ScoreboardUtil> sc;
	 
	 private static ScoreboardManagers sm;
	 
	 public static ScoreboardManagers getInstance() {
	  if (sm == null)
	   sm = new ScoreboardManagers();
	  return sm;
	 }
	 
	 public void addScoreboard(String name, ScoreboardUtil su) {
	  getScoreboards().put(name, su);
	 }
	 
	 public HashMap<String, ScoreboardUtil> getScoreboards() {
	  if (sc == null)
	   sc = new HashMap<String, ScoreboardUtil>();
	  return sc;
	 }
	 
	 public ScoreboardUtil getScoreboard(String name) {
	  if (!hasScoreboard(name)) return new ScoreboardUtil(name, true);
	  return getScoreboards().get(name);
	 }
	 
	 public boolean hasScoreboard(String name) {
	  if (getScoreboards().get(name) != null) 
	   return true;
	  return false;
	 }
	}
