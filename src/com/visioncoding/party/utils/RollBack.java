package com.visioncoding.party.utils;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

import com.visioncoding.party.SpikyParty;

public class RollBack {
	
	private SpikyParty main;
	
	public RollBack(SpikyParty main) {
		this.main = main;
	}
	
	public void unloadMap(String mapName){ 
	      if(Bukkit.getServer().unloadWorld(Bukkit.getServer().getWorld(mapName), false)){ 
	             this.main.getLogger().info("Successfully unloaded: " + mapName); 
	      } else { 
	    	  this.main.getLogger().severe("Unable to unload: " + mapName); 
	         } 
	    } 
	    
	public void loadMap(String mapname){ 
	      Bukkit.getServer().createWorld(new WorldCreator(mapname)); 
	 } 
 
	public void rollback(String mapname){ 
	     unloadMap(mapname); 
	     loadMap(mapname); 
	  } 

}
