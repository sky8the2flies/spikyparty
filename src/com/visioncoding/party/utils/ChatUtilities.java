package com.visioncoding.party.utils;

import static org.bukkit.ChatColor.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtilities {
	
	@SuppressWarnings("deprecation")
	public static void Broadcast(String msg){
		for( Player player : Bukkit.getOnlinePlayers())
			player.sendMessage(starter() + msg);
	}
	
	private static String starter() {
		return GRAY + "[" + RED + "SpikyParty" + GRAY + "] ";
	}

	public static void sendMessage(Player player, String msg) {
		player.sendMessage(starter() + msg);
		
	}
	
	public static String format(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

}
