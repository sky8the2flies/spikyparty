package com.visioncoding.party.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import com.visioncoding.party.game.MapType;
import com.visioncoding.party.lobby.Lobby;
import com.visioncoding.party.utils.ChatUtilities;

public class CountdownTask implements Runnable{
	

	 private int time = 70;
	 
	
	 
	 private int taskId = 0;
	 
	 @SuppressWarnings({ "unused", "deprecation" })
	 public void run() {
	  int cur = time;
	  if (cur % 60 == 0 && cur != 0) {
		  ChatUtilities.Broadcast(ChatColor.RED + "" + cur / 60 + ChatColor.GRAY + " minute" + (cur/60 == 1 ? "" : "s") + " until the game starts!");
	   
	  }
	  
	  if (cur % 10 == 0) {
	   if (!(Bukkit.getOnlinePlayers().length >= 4)) {
	    this.time = 70;
	   }
	  }
	  
	  if (cur <= 30 && cur % 10 == 0 && cur > 0) {
		  ChatUtilities.Broadcast(ChatColor.RED + "" + cur + ChatColor.GRAY + " second" + (cur == 1 ? "" : "s") + " until the game starts!");
	  }
	  
	  if (cur < 10 && cur > 0) {
		  if (Lobby.jungle > Lobby.candy) {
			  if (Lobby.jungle > Lobby.rainbow) {
				  if (Lobby.jungle > Lobby.sandville) {
					  if (Lobby.jungle > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Jungle has won!");
						  MapType.setMap(MapType.JUNGLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.candy > Lobby.jungle) {
			  if (Lobby.candy > Lobby.rainbow) {
				  if (Lobby.candy > Lobby.sandville) {
					  if (Lobby.candy > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Candy has won!");
						  MapType.setMap(MapType.CANDY);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.rainbow > Lobby.candy) {
			  if (Lobby.rainbow > Lobby.jungle) {
				  if (Lobby.rainbow > Lobby.sandville) {
					  if (Lobby.rainbow > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Rainbow has won!");
						  MapType.setMap(MapType.RAINBOW);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.sandville > Lobby.candy) {
			  if (Lobby.sandville > Lobby.rainbow) {
				  if (Lobby.sandville > Lobby.jungle) {
					  if (Lobby.sandville > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. SandVille has won!");
						  MapType.setMap(MapType.SANDVILLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.skypia > Lobby.candy) {
			  if (Lobby.skypia > Lobby.rainbow) {
				  if (Lobby.skypia > Lobby.sandville) {
					  if (Lobby.skypia > Lobby.jungle) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Skypia has won!");
						  MapType.setMap(MapType.SKYPIA);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.jungle == Lobby.candy) {
			  if (Lobby.jungle == Lobby.rainbow) {
				  if (Lobby.jungle == Lobby.sandville) {
					  if (Lobby.jungle == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Jungle has won!");
						  MapType.setMap(MapType.JUNGLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.jungle > Lobby.candy) {
			  if (Lobby.jungle == Lobby.rainbow) {
				  if (Lobby.jungle == Lobby.sandville) {
					  if (Lobby.jungle == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Jungle has won!");
						  MapType.setMap(MapType.JUNGLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.jungle == Lobby.candy) {
			  if (Lobby.jungle > Lobby.rainbow) {
				  if (Lobby.jungle == Lobby.sandville) {
					  if (Lobby.jungle == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Jungle has won!");
						  MapType.setMap(MapType.JUNGLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.jungle == Lobby.candy) {
			  if (Lobby.jungle == Lobby.rainbow) {
				  if (Lobby.jungle > Lobby.sandville) {
					  if (Lobby.jungle == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Jungle has won!");
						  MapType.setMap(MapType.JUNGLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.jungle == Lobby.candy) {
			  if (Lobby.jungle == Lobby.rainbow) {
				  if (Lobby.jungle == Lobby.sandville) {
					  if (Lobby.jungle > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Jungle has won!");
						  MapType.setMap(MapType.JUNGLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.candy > Lobby.jungle) {
			  if (Lobby.candy == Lobby.rainbow) {
				  if (Lobby.candy == Lobby.sandville) {
					  if (Lobby.candy == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Candy has won!");
						  MapType.setMap(MapType.CANDY);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.candy == Lobby.jungle) {
			  if (Lobby.candy > Lobby.rainbow) {
				  if (Lobby.candy == Lobby.sandville) {
					  if (Lobby.candy == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Candy has won!");
						  MapType.setMap(MapType.CANDY);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.candy == Lobby.jungle) {
			  if (Lobby.candy == Lobby.rainbow) {
				  if (Lobby.candy > Lobby.sandville) {
					  if (Lobby.candy == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Candy has won!");
						  MapType.setMap(MapType.CANDY);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.candy == Lobby.jungle) {
			  if (Lobby.candy == Lobby.rainbow) {
				  if (Lobby.candy == Lobby.sandville) {
					  if (Lobby.candy > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Candy has won!");
						  MapType.setMap(MapType.CANDY);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.rainbow > Lobby.jungle) {
			  if (Lobby.rainbow == Lobby.candy) {
				  if (Lobby.rainbow == Lobby.sandville) {
					  if (Lobby.rainbow == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Rainbow has won!");
						  MapType.setMap(MapType.RAINBOW);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.rainbow == Lobby.jungle) {
			  if (Lobby.rainbow > Lobby.candy) {
				  if (Lobby.rainbow == Lobby.sandville) {
					  if (Lobby.rainbow == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Rainbow has won!");
						  MapType.setMap(MapType.RAINBOW);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.rainbow == Lobby.jungle) {
			  if (Lobby.rainbow == Lobby.candy) {
				  if (Lobby.rainbow > Lobby.sandville) {
					  if (Lobby.rainbow == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Rainbow has won!");
						  MapType.setMap(MapType.RAINBOW);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.rainbow == Lobby.jungle) {
			  if (Lobby.rainbow == Lobby.candy) {
				  if (Lobby.rainbow == Lobby.sandville) {
					  if (Lobby.rainbow > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Rainbow has won!");
						  MapType.setMap(MapType.RAINBOW);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.sandville > Lobby.jungle) {
			  if (Lobby.sandville == Lobby.candy) {
				  if (Lobby.sandville == Lobby.rainbow) {
					  if (Lobby.sandville == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. SandVille has won!");
						  MapType.setMap(MapType.SANDVILLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.sandville == Lobby.jungle) {
			  if (Lobby.sandville > Lobby.candy) {
				  if (Lobby.sandville == Lobby.rainbow) {
					  if (Lobby.sandville == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. SandVille has won!");
						  MapType.setMap(MapType.SANDVILLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.sandville == Lobby.jungle) {
			  if (Lobby.sandville == Lobby.candy) {
				  if (Lobby.sandville > Lobby.rainbow) {
					  if (Lobby.sandville == Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. SandVille has won!");
						  MapType.setMap(MapType.SANDVILLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.sandville == Lobby.jungle) {
			  if (Lobby.sandville == Lobby.candy) {
				  if (Lobby.sandville == Lobby.rainbow) {
					  if (Lobby.sandville > Lobby.skypia) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. SandVille has won!");
						  MapType.setMap(MapType.SANDVILLE);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.skypia > Lobby.jungle) {
			  if (Lobby.skypia == Lobby.candy) {
				  if (Lobby.skypia == Lobby.rainbow) {
					  if (Lobby.skypia == Lobby.sandville) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Skypia has won!");
						  MapType.setMap(MapType.SKYPIA);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.skypia == Lobby.jungle) {
			  if (Lobby.skypia > Lobby.candy) {
				  if (Lobby.skypia == Lobby.rainbow) {
					  if (Lobby.skypia == Lobby.sandville) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Skypia has won!");
						  MapType.setMap(MapType.SKYPIA);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.skypia == Lobby.jungle) {
			  if (Lobby.skypia == Lobby.candy) {
				  if (Lobby.skypia > Lobby.rainbow) {
					  if (Lobby.skypia == Lobby.sandville) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Skypia has won!");
						  MapType.setMap(MapType.SKYPIA);
					  }
				  }
			  }
		  }
		  
		  if (Lobby.skypia == Lobby.jungle) {
			  if (Lobby.skypia == Lobby.candy) {
				  if (Lobby.skypia == Lobby.rainbow) {
					  if (Lobby.skypia > Lobby.sandville) {
						  ChatUtilities.Broadcast(ChatColor.GREEN + "The voting has ended. Skypia has won!");
						  MapType.setMap(MapType.SKYPIA);
					  }
				  }
			  }
		  }
		  ChatUtilities.Broadcast(ChatColor.RED + "" + cur + ChatColor.GRAY + " second" + (cur == 1 ? "" : "s") + " until the game starts!");
	  }
	  
	  if (cur == 0) {
	
	   Bukkit.getScheduler().cancelTask(taskId());
	   //start first game
	  
	  }
	  
	  for (Player p :Bukkit.getOnlinePlayers()) {
		  p.setLevel(cur);
		  p.setExp(0);
		  
		  ScoreboardManager manager = Bukkit.getScoreboardManager();
			 Scoreboard board = manager.getNewScoreboard();
			 Team team = board.registerNewTeam("scoreboard");
			 Objective obj = board.registerNewObjective("board", "dummy");
			 obj.setDisplaySlot(DisplaySlot.SIDEBAR);
			 if (Bukkit.getOnlinePlayers().length == 1) {
			 obj.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Waiting For " + "3 " + "players");
			 } else if (Bukkit.getOnlinePlayers().length == 2) {
				 obj.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Waiting For " + "2 " + "players");
				 
			 } else if (Bukkit.getOnlinePlayers().length == 2) {
				 obj.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Waiting For " + "1 " + "player");
				 	
			 } else {
				 obj.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "Starting In " + ChatColor.AQUA + cur + " Seconds" );
			 }
			 
			 Score score = obj.getScore(Bukkit.getOfflinePlayer("   "));
			 score.setScore(3);

			 Score score1 = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GOLD + "" + ChatColor.BOLD + "Players"));
			 score1.setScore(2);
			 
			 Score score2 = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.WHITE + "" + Bukkit.getOnlinePlayers().length + "/6"));
			 score2.setScore(1);
			 
			
			 
		
			 p.setScoreboard(board);
			 
			 if (cur == 0) {
				 p.setScoreboard(manager.getNewScoreboard());
			 }
	  }
	  
	  
	  time--;
	 }
	 
	 public Integer taskId() {
	  return taskId;
	 }
	 
	 public void setTaskId(int taskId) {
	  this.taskId = taskId;
	 }
	 
	 

	
	
}
			

