package com.visioncoding.party.lobby;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.visioncoding.party.SpikyParty;
import com.visioncoding.party.game.GameManager;
import com.visioncoding.party.game.GameType;
import com.visioncoding.party.utils.ChatUtilities;
import com.visioncoding.party.utils.ItemUtil;

public class Lobby implements Listener {
	
	public static int jungle = 0;
	public static int candy = 0;
	public static int sandville = 0;
	public static int skypia = 0;
	public static int rainbow = 0;
	
	Inventory maps = Bukkit.createInventory(null, 27, ChatColor.AQUA + "Map Voting");
	ArrayList<String> votedmap = new ArrayList<String>();
	ArrayList<String> voteend = new ArrayList<String>();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		if (GameManager.get().getGame().getType() == GameType.WAITING) {
		event.setJoinMessage(ChatColor.YELLOW + player.getName() + ChatColor.GREEN + "has joined the game " + ChatColor.YELLOW + "(" + Bukkit.getServer().getOnlinePlayers().length + "/6)");
		
		player.getInventory().clear();
		player.getInventory().setHelmet(new ItemStack(Material.AIR));
		player.getInventory().setChestplate(new ItemStack(Material.AIR));
		player.getInventory().setLeggings(new ItemStack(Material.AIR));
		player.getInventory().setBoots(new ItemStack(Material.AIR));
		player.setGameMode(GameMode.SURVIVAL);
		
		ItemStack mapvote = ItemUtil.createItemStack(new ItemStack(Material.PAPER), ChatColor.GREEN + "Map Selector", ChatColor.GRAY + "Right-click to select a map to play!");
		player.getInventory().setItem(0, mapvote);
		player.updateInventory();
		
		SpikyParty.getPlayers().addPlayer(player);
		}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		
		event.setQuitMessage(ChatColor.YELLOW + player.getName() + ChatColor.RED + "has left the game " + ChatColor.YELLOW + "(" + Bukkit.getServer().getOnlinePlayers().length + "/6)");
		
		if (SpikyParty.getPlayers().isPlayerPlaying(player)) {
		SpikyParty.getPlayers().removePlayer(player);
		} else {
			SpikyParty.getSpectators().removePlayer(player);
		}
		
	}
	
	@EventHandler
	public void onLogin(AsyncPlayerPreLoginEvent event) {
		if (!(GameManager.get().getGame().getType() == GameType.WAITING)) {
			event.disallow(Result.KICK_OTHER, ChatColor.RED + "Game has already started, please join another!");
		}
	}
	
	@EventHandler
	public void PlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Material mat = player.getItemInHand().getType();
		
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			if(mat == Material.PAPER) {
				openMapVoting(player);
			}
		}
	}
	
	public void openMapVoting(Player player) {
		
		ItemStack jungle = ItemUtil.createItemStack(new ItemStack(Material.PAPER), ChatColor.GREEN + "Jungle", ChatColor.GRAY + "Click to vote for this map!");
		maps.setItem(0, jungle);
		ItemStack candy = ItemUtil.createItemStack(new ItemStack(Material.PAPER), ChatColor.GREEN + "Candy", ChatColor.GRAY + "Click to vote for this map!");
		maps.setItem(1, candy);
		ItemStack sandville = ItemUtil.createItemStack(new ItemStack(Material.PAPER), ChatColor.GREEN + "SandVille", ChatColor.GRAY + "Click to vote for this map!");
		maps.setItem(2, sandville);
		ItemStack skypia = ItemUtil.createItemStack(new ItemStack(Material.PAPER), ChatColor.GREEN + "Skypia", ChatColor.GRAY + "Click to vote for this map!");
		maps.setItem(3, skypia);
		ItemStack rainbow = ItemUtil.createItemStack(new ItemStack(Material.PAPER), ChatColor.GREEN + "Rainbow", ChatColor.GRAY + "Click to vote for this map!");
		maps.setItem(4, rainbow);
		
		player.openInventory(maps);
	}
	
	 @EventHandler
		public void onDonatorBoxGUIClick(InventoryClickEvent event) {
			Player player = (Player) event.getWhoClicked();
			
			if (event.getInventory().getName().equalsIgnoreCase(ChatColor.AQUA + "Map Voting")) {
			
				if (event.getCurrentItem().getType() == Material.PAPER) {
					
					if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Jungle")) {
						if (!votedmap.contains(player.getName())) {
							if (!voteend.contains(player.getName())) {
							jungle++;
							votedmap.add(player.getName());
							ChatUtilities.sendMessage(player, ChatColor.GRAY + "You have voted for " + ChatColor.YELLOW + "Jungle " + ChatColor.GRAY + "! (" + jungle + ChatColor.GRAY + " votes)");
							} else {
								ChatUtilities.sendMessage(player, ChatColor.RED + "The voting phase has already ended!");
							}
						} else {
							ChatUtilities.sendMessage(player, ChatColor.RED + "You have already voted for a map!");
						}
					}
				}
				
				if (event.getCurrentItem().getType() == Material.PAPER) {
					
					if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Candy")) {
						if (!votedmap.contains(player.getName())) {
							if (!voteend.contains(player.getName())) {
							candy++;
							votedmap.add(player.getName());
							ChatUtilities.sendMessage(player, ChatColor.GRAY + "You have voted for " + ChatColor.YELLOW + "Candy " + ChatColor.GRAY + "! (" + candy + ChatColor.GRAY + " votes)");
							} else {
								ChatUtilities.sendMessage(player, ChatColor.RED + "The voting phase has already ended!");
							}
						} else {
							ChatUtilities.sendMessage(player, ChatColor.RED + "You have already voted for a map!");
						}
					}
				}

				if (event.getCurrentItem().getType() == Material.PAPER) {
	
					if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "SandVille")) {
						if (!votedmap.contains(player.getName())) {
							if (!voteend.contains(player.getName())) {
								sandville++;
								votedmap.add(player.getName());
								ChatUtilities.sendMessage(player, ChatColor.GRAY + "You have voted for " + ChatColor.YELLOW + "SandVille " + ChatColor.GRAY + "! (" + sandville + ChatColor.GRAY + " votes)");
							} else {
								ChatUtilities.sendMessage(player, ChatColor.RED + "The voting phase has already ended!");
							}
						} else {
							ChatUtilities.sendMessage(player, ChatColor.RED + "You have already voted for a map!");
							}
						}
					}
				
				if (event.getCurrentItem().getType() == Material.PAPER) {
					
					if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Skypia")) {
						if (!votedmap.contains(player.getName())) {
							if (!voteend.contains(player.getName())) {
								skypia++;
								votedmap.add(player.getName());
								ChatUtilities.sendMessage(player, ChatColor.GRAY + "You have voted for " + ChatColor.YELLOW + "Skypia " + ChatColor.GRAY + "! (" + skypia + ChatColor.GRAY + " votes)");
							} else {
								ChatUtilities.sendMessage(player, ChatColor.RED + "The voting phase has already ended!");
							}
						} else {
							ChatUtilities.sendMessage(player, ChatColor.RED + "You have already voted for a map!");
							}
						}
					}
				
				if (event.getCurrentItem().getType() == Material.PAPER) {
					
					if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Rainbow")) {
						if (!votedmap.contains(player.getName())) {
							if (!voteend.contains(player.getName())) {
								rainbow++;
								votedmap.add(player.getName());
								ChatUtilities.sendMessage(player, ChatColor.GRAY + "You have voted for " + ChatColor.YELLOW + "Rainbow " + ChatColor.GRAY + "! (" + rainbow + ChatColor.GRAY + " votes)");
							} else {
								ChatUtilities.sendMessage(player, ChatColor.RED + "The voting phase has already ended!");
							}
						} else {
							ChatUtilities.sendMessage(player, ChatColor.RED + "You have already voted for a map!");
							}
						}
					}
				
				
				
			}
	 }
			
	
	

}
