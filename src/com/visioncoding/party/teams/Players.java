package com.visioncoding.party.teams;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Players {
	
	private static List<Player> players = new ArrayList<Player>();
	
	public void addPlayer(Player player) {
		if (players.contains(player)) {
			return;
		}
		
		players.add(player);
		player.setFlying(false);
		player.setAllowFlight(false);
		player.setDisplayName(ChatColor.GREEN + player.getName());
		player.sendMessage(ChatColor.GRAY + "You are now on the " + ChatColor.YELLOW + "Red Team" + ChatColor.GRAY + " team!");
	}
	
	public List<Player> getPlaying() {
		return players;
	}
	
	public boolean isPlayerPlaying(Player player){
        if (players.contains(player)){
            return true;
        }
        else {
            return false;
        }
    }
	
	public void removePlayer(Player player) {
		if (players.contains(player)) {
			players.remove(player);
		}
	}

	

}
