package com.visioncoding.party.teams;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Spectators {
	
	private static List<Player> spectators = new ArrayList<Player>();
	
	public void addPlayer(Player player) {
		if (spectators.contains(player)) {
			return;
		}
		
		spectators.add(player);
		player.setFlying(true);
		player.setAllowFlight(true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 99999999, 9999999));
		player.sendMessage(ChatColor.GRAY + "You are now on the " + ChatColor.YELLOW + "Blue Team" + ChatColor.GRAY + " team!");
	}
	
	public List<Player> getPlaying() {
		return spectators;
	}
	
	public boolean isSpectatorSpectating(Player player){
        if (spectators.contains(player)){
            return true;
        }
        else {
            return false;
        }
    }
	
	public void removePlayer(Player player) {
		if (spectators.contains(player)) {
			spectators.remove(player);
			player.removePotionEffect(PotionEffectType.INVISIBILITY);
		}
	}

	

}
