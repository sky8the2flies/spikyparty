package com.visioncoding.party;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.visioncoding.party.tasks.CountdownTask;
import com.visioncoding.party.teams.Players;
import com.visioncoding.party.teams.Spectators;

public class SpikyParty extends JavaPlugin implements Listener {
	
	public static SpikyParty instance;
	
	private static Players players = new Players();
	private static Spectators spectators = new Spectators();
	
	public void onEnable() {
		super.onEnable();
		
		CountdownTask task = new CountdownTask();
		
		task.setTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(this, task, 20L, 20L));
		
		instance = this;
		saveDefaultConfig();
		
		registerListeners();
	}
	//
	public static SpikyParty get() {
		return instance;
	}
	
	public void registerListeners() {
		Bukkit.getPluginManager().registerEvents(this, this);
	}
	
	

	public static Players getPlayers() {
		return players;
	}
	
	public static Spectators getSpectators() {
		return spectators;
	}
	
	
	
	
	
	public void onDisable() {	
		//above code here
		super.onDisable();
		
	}
	
	


}
